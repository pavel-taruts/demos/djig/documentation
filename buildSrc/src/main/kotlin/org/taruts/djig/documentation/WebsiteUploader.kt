package org.taruts.djig.documentation

import software.amazon.awssdk.auth.credentials.ProfileCredentialsProvider
import software.amazon.awssdk.regions.Region
import software.amazon.awssdk.services.s3.S3Client
import software.amazon.awssdk.services.s3.model.*
import java.io.File
import java.net.URI
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.attribute.BasicFileAttributes
import java.util.function.Function
import java.util.stream.Collectors
import java.util.stream.StreamSupport

object WebsiteUploader {

    public fun upload(bucketName: String, directory: File) {
        val s3: S3Client = getS3Client()
        s3.use {
            refreshObjects(s3, bucketName, directory)
        }
    }

    private fun getS3Client(): S3Client {
        val credentialsProvider = ProfileCredentialsProvider.create()

        val endpoint: String = System.getProperty("s3.endpoint")
        val region: String = System.getProperty("s3.region")

        val s3 = S3Client.builder()
            .endpointOverride(URI(endpoint))
            .region(Region.of(region))
            .credentialsProvider(credentialsProvider)
            .build()

        return s3
    }

    private fun refreshObjects(s3: S3Client, bucketName: String, directory: File) {
        deleteExistingObjects(s3, bucketName)
        uploadFiles(s3, bucketName, directory)
    }

    private fun deleteExistingObjects(s3: S3Client, bucketName: String) {
        println("Getting list of existing objects...")

        val objects: List<S3Object> = listObjects(s3, bucketName)

        println("Deleting...")

        val objectIdentifiers = objects
            .stream()
            .map(S3Object::key)
            .map(
                Function { key: String? ->
                    ObjectIdentifier
                        .builder()
                        .key(key)
                        .build()
                }
            )
            .collect(Collectors.toList())

        val deleteObjectsRequest = DeleteObjectsRequest
            .builder()
            .bucket(bucketName)
            .delete(
                Delete
                    .builder()
                    .objects(objectIdentifiers)
                    .build()
            )
            .build()

        s3.deleteObjects(deleteObjectsRequest)

        println("Deleting... Done")
    }

    private fun listObjects(
        s3: S3Client,
        bucketName: String
    ): List<S3Object> {
        val listObjectsRequest = ListObjectsRequest
            .builder()
            .bucket(bucketName)
            .build()

        val listObjectsResponse: ListObjectsResponse = s3.listObjects(listObjectsRequest)

        return listObjectsResponse.contents()
    }

    private fun uploadFiles(s3: S3Client, bucketName: String, directory: File) {
        val directoryPath = directory.toPath().toAbsolutePath().normalize()
        Files
            .find(
                directoryPath, Int.MAX_VALUE,
                { _: Path, fileAttr: BasicFileAttributes ->
                    fileAttr.isRegularFile
                }
            )
            .forEach { filePath: Path ->
                val key = getKey(directoryPath, filePath)
                println("Uploading object $key...")
                putS3Object(s3, bucketName, key, filePath)
            }
    }

    private fun getKey(directoryPath: Path, filePath: Path): String {
        val relativePath = directoryPath.relativize(filePath.toAbsolutePath().normalize())
        return StreamSupport
            .stream(relativePath.spliterator(), false)
            .map(Function { obj: Path -> obj.toString() })
            .collect(Collectors.joining("/"))
    }

    fun putS3Object(s3: S3Client, bucketName: String?, objectKey: String?, objectPath: Path?) {
        val putOb = PutObjectRequest.builder()
            .bucket(bucketName)
            .key(objectKey)
            .build()
        s3.putObject(putOb, objectPath)
    }
}
