import org.taruts.djig.documentation.WebsiteUploader

repositories {
    mavenLocal()
    mavenCentral()
}

tasks.register("buildSite", Exec::class.java) {
    group = "djig documentation"
    commandLine = listOf("mkdocs", "build")
}

tasks.register("uploadSite") {
    group = "djig documentation"

    dependsOn.add("buildSite")

    doLast {
        val siteDirectory: File = project.file("site")
        WebsiteUploader.upload("djig.org", siteDirectory)
    }
}
