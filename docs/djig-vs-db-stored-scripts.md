# Djig vs Scripts in DB

Djig is an alternative to storing dynamic code (written in Groovy, JavaScript etc.) in a database, 
and has the following advantages in comparison to the DB approach:

## 1. Better management of code changes with Git

Djig uses Git, 
and Git has a lot of functionality 
to help developers to deal with code changes, like the following:

- Tracking
- Analysis
- Rollbacks
- Group development etc.

## 2. Leveraging IDEs

What follows from using Git and not using DBs is that dynamic code is stored and edited in plain old files, 
which allows working with them using IDEs, enjoying all the services they provide, like: 

- Code navigation
- Syntax highlighting
- Automated refactoring
- Inspections

## 3. Better control over developers' local dynamic code

Whether dynamic code is in a DB or is managed by djig, 
developers often have their own versions of dynamic code which they use locally.

Versions of dynamic code that different developers have are kind of similar, but might differ in some ways.

Using Git instead of a DB, in particular its branching mechanism, 
help developers share their versions to one another, 
decreases the number of such variations, 
and makes creation of a personal dynamic code version easier for a new developer, 
when they start working in the team.

Git branches also make it easier to switch between dynamic code variations. 
