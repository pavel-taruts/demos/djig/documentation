# Information for Developers

The framework consists of quite a few Git repositories.

Those repositories are organized with a help of 
the [org.taruts.workspace Gradle plugin](https://plugins.gradle.org/plugin/org.taruts.workspace).

This means, that you don't need to clone them manually, one by one.

Clone just this one:
<https://gitlab.com/pavel-taruts/djig/workspace>.

After that run `gradle cloneAll` in the local directory that was created.

Here's the full script for Linux and Windows.

###### Linux

```bash
clone https://gitlab.com/pavel-taruts/djig/workspace djig
cd djig
sh ./gradlew cloneAll
```

###### Windows

```bash
clone https://gitlab.com/pavel-taruts/djig/workspace djig
cd djig
gradlew cloneAll
```

The cloned projects will be in `djig/projects`.
