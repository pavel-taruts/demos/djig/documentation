# Example Djig Application

There's an example djig application being a part of the djig project.

It consists of these Git repositories:

- <https://gitlab.com/pavel-taruts/djig/example/app>
- <https://gitlab.com/pavel-taruts/djig/example/dynamic-api>
- <https://gitlab.com/pavel-taruts/djig/example/dynamic-dev>

As you can see, they are in the same project group 
<https://gitlab.com/pavel-taruts/djig/example>.

Those repositories are similar to those from the [Tutorial](tutorial.md), 
but they are somehow different.
You wouldn't be able to run them directly, because you have read-only access to them, 
and it would create problems.

To run them and see how dynamic code is redeployed at runtime, you'll need your own copies of the projects.
You'll also need to change the references between the projects in the dependencies and Spring Boot configuration properties.

Aside from that you'll need to specify your credentials for GitLab 
[in the Spring Boot configuration properties](architecture/dynamic-project.md#dynamic-project-configuration-properties). 
