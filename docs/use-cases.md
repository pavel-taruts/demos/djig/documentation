# Possible Use Cases of Djig

Dynamic code development can be delegated to people 
who are not software developers 
but rather advanced users of the system with some programming skills.

The task of users like that could be flexible configuration of the product in realtime, 
according to the situation.

For example, such advanced users could use dynamic code to define stock trading strategies.

Or dynamic code could be used to alter the application behaviour 
when parts of the system stop working, 
so that the application continues being usable, 
though with limited functionality.

Dynamic code can be used not only for code intended to be constantly changed at runtime.
There might be code that is normally modified using a normal CI/CD flow, 
but that must have an option to be changed at runtime in some special cases.

Also, djig can be used to make development of normal Spring Boot components faster, 
if the development process requires many cycles of rebuilding 
and restarting the application locally, and it takes a lot of time.
In this case you can make a component under development to be dynamic 
just for a time and make it back a normal component afterwards.
