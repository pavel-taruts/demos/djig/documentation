# Djig Gradle plugin

This plugin helps djig application developers create their own personal local copies 
of the dynamic projects of the app.

The personal versions are created 
by copying common versions of dynamic projects, 
which are used by all developers in the team.

Aside from copying dynamic projects, 
the plugin also creates a Spring Boot profile 
to run the djig application 
using the newly created personal dynamic projects.

The copying can be made either to separate repositories, 
automatically created in the process, 
or to separate branches of the source repositories 
(the branches are then created automatically as well)

Also, the copying can be made either inside one Git repository hosting or between hostings, 
like from one GitLab instance to another, from GitLab to GitHub, etc. 

You can read more about the features of the plugin and its configuration in the documentation in the extension source code:

- [DjigPluginExtension.kt](https://gitlab.com/pavel-taruts/gradle-plugins/djig-gradle-plugin/-/blob/master/src/main/kotlin/org/taruts/djigGradlePlugin/extension/DjigPluginExtension.kt)
- [TaskDescription.kt](https://gitlab.com/pavel-taruts/gradle-plugins/djig-gradle-plugin/-/blob/master/src/main/kotlin/org/taruts/djigGradlePlugin/extension/tasks/TaskDescription.kt)
