# Djig vs Microservices

The idea of djig is somehow similar to the idea of microservices.

Like a microservice, a dynamic component also has a separate flow of development and deployment, 
and redeploying a microservice also does not require restarting the whole application.

The difference is that dynamic components work on the same computer with the main part of the application,
and they do not require a separate instance of Spring Boot. 

However, the idea of using djig does not contradict that of using microservices, 
those two approaches can be used together in one system. 
The main advantage of djig compared to microservices is the deployment speed.
When the deployment speed is important you can use djig dynamic beans, 
while at the same time arranging some other code as microservices.
