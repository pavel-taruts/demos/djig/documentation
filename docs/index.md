# Welcome to Djig!

Djig means **D**ynamic **J**ava **I**n **G**it. 
It is a framework enabling Spring Boot applications to quickly renew beans without restart.

The source code of **dynamic beans** like that is stored separately from the main Git repository of the application, 
in a separate Git repository.

An application can have several of such repositories, 
each containing a group of dynamic beans. 

A Git repository with the source code of dynamic beans is called a **dynamic repository** or **dynamic project**.

When a developer decides to change dynamic bean code,
they do the changes, push them, and the bean inside the running Spring Boot application 
is refreshed automatically in two or three seconds.

Dynamic beans are full-fledged Spring Boot beans.
Via the standard dependency injection mechanism they can use 
whatever components or configuration properties of the main part of the application they need. 

In the Spring Boot application, 
dynamic beans coming from one dynamic project 
are arranged as a **dynamic child context**.

A dynamic project can depend on libraries that the main part of the application
does not depend on. Libraries like that are called **dynamic dependencies**.

Also, a dynamic project can add its own REST endpoint to the application (**dynamic REST endpoints**).

To add, change or delete dynamic dependencies or endpoints also does not require a restart of the application.  

A dynamic project can replace beans and configuration properties of the main part of the application for its internal use.

Djig supports dynamic projects written in **Java** or **Kotlin** 
stored on **GitHub** or **GitLab** (including **private GitLab instances**).  

As a build system for dynamic projects djig supports **Maven** and **Gradle**.
