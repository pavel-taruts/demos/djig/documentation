# Dynamic Projects and Configuration Properties of the Application

Because a [DynamicApplicationContext](DynamicApplicationContext.md) instance 
is a child context of the main context of the application, 
it can access all configuration properties of the main part of the application.

It means that you can use  
`@Value` and `@ConfigurationProperties`
in dynamic project beans to inject their properties 
with configuration property values of the main part of the application.

This also implies that configuration properties needed by a dynamic project
can be arranged as configuration properties of the main part of the application.

Aside from that, a dynamic project can have its own configuration properties 
(e.g. in the `application.yaml` resource).

Such internal configuration properties have priority over those of the main context and can override them.
