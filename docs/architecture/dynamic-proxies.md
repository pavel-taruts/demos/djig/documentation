# Dynamic CGLIB proxies

As early as at the application startup, 
the djig runtime knows the full list of the dynamic beans there will be, 
and their interfaces.

At runtime, the implementations will be able to change, 
but the number of dynamic beans and their interfaces will remain the same.

For every dynamic bean at startup a CGLIB proxy is created, 
which implements the dynamic bean interface, as does the bean itself.

Each proxy like that has a delegate field of the same dynamic bean interface type, 
and all the proxy does is forwarding all interface calls 
to exactly those calls on the delegate.

Those proxies are created at startup and are used all the way on,
staying the same even when the dynamic code changes.

When the dynamic code changes, the djig runtime creates new versions of dynamic beans
and sets them as the delegates to the corresponding CGLIB proxies.

Each of the proxies is a normal bean of the main Spring Boot context.

It's the CGLIB proxy that is injected into a bean not the dynamic bean itself. 
The dynamic bean is the delegate of the proxy.

See implementation details in the
[org.taruts.djig.core.mainContext.proxy](https://gitlab.com/pavel-taruts/djig/spring-boot-starter/-/tree/master/src/main/java/org/taruts/djig/core/mainContext/proxy)
package
of the
[djig-spring-boot-starter](https://gitlab.com/pavel-taruts/djig/spring-boot-starter)
project.
