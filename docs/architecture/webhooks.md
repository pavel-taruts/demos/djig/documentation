[DynamicClassLoader]: DynamicClassLoader.md
[DynamicApplicationContext]: DynamicApplicationContext.md
[dynamic proxies]: dynamic-proxies.md

[port forwarding]: https://en.wikipedia.org/wiki/Port_forwarding
[reverse proxy]: https://ru.wikipedia.org/wiki/Обратный_прокси
[nginx]: https://nginx.org/

# Webhooks and Webhook Endpoints

At application startup, djig registers webhooks at the Git repository hostings where the dynamic projects of the app are stored.
This is done using the REST APIs of the hostings.

After that, on every change to the dynamic code, 
a callback request comes to a specific endpoint known to djig, 
which djig has set up with the webhook at startup.

The endpoint controller then determines the dynamic project having been changed,
downloads the new version of the code,
builds the project, 
and uses the resulting `.class` files and the dependency JAR URLs 
to create a new [DynamicClassLoader] and a new [DynamicApplicationContext].

Then the [dynamic proxies]
that were created in the main context for dynamic beans of this dynamic project,
are switched to the beans of the newly created [DynamicApplicationContext].

The old [DynamicApplicationContext]
loses its connection with the [dynamic proxies] 
and with the djig runtime in general 
and will be removed by the garbage collector.

## Webhook Configuration Properties

The base URL for the callback requests is specified by these properties:

- `djig.hook.host`
- `djig.hook.port`
- `djig.hook.protocol`
- `djig.controller.refresh.path`

For GitLab the full URL is calculated in this way:

- `<protocol>://<host>:<port>/<djig.controller.refresh.path>/generic/<projectName>`

For GitHub the full URL is calculated in this way:

- `<protocol>://<host>:<port>/<djig.controller.refresh.path>/github`

<a name="indirect-webhooks"></a>
## Configuration if the Network Interface is Inaccessible from the Public Internet Directly

In our case a webhook callback request is when the Git repository hosting sends an HTTP request to a djig application.

A request like this comes at a port of a network interface, 
where the djig application receives TCP connections.

Here there might be a nuance that the network interface is not accessible from the hosting directly,
because the local subnet of the interface is not.

In cases like this, techniques of indirect access are used, such as 
[port forwarding] и [reverse proxy] (e.g. [nginx]).

[Port forwarding], for example,
is often used for external access to home computers, 
connected to the internet via a private local network behind a home router.

In commercial networks businesses often use the [reverse proxy] approach.

Whatever your case may be, incoming TCP connections and incoming HTTP requests will come 
not at the IP address where the djig app listens.

In the case of [port forwarding] the requests 
will have to be sent at the IP address of the router and at the forwarded port.
The properties `djig.hook.host`, `djig.hook.port` will then have to be the host and port of the router, 
and the `djig.hook.protocol` will have to be `http`.

In the case of [reverse proxy] 
the HTTP requests will have to be sent at the host and port of the [reverse proxy]. 
Aside from that, the proxy can be accepting only HTTPS requests, 
communicating with the application via HTTP.
The properties `djig.hook.host`, `djig.hook.port` and `djig.hook.protocol` 
will have to be the host, port and protocol of the proxy.
