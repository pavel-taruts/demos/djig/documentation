[DynamicClassLoader]: https://gitlab.com/pavel-taruts/djig/spring-boot-starter/-/blob/master/src/main/java/org/taruts/djig/core/childContext/classLoader/DynamicClassLoader.java
[API JAR]: api-jar.md

# DynamicClassLoader

On every change in a dynamic project, 
the djig runtime downloads the new version of the code, 
builds it, 
and, as the result of the build, 
the `.class` files appear, 
and their dependencies are discovered.

The dependencies are `.jar` files. By default, Maven downloads them to `~/.m2/repository/` 
and Gradle downloads them to `~/.gradle/caches/modules-2/files-2.1/`.

Those `.class` and `.jar` files are used to create a new instance of 
[DynamicClassLoader]. This is the `ClassLoader` that will load all classes for the dynamic project.
It's a child `ClassLoader` of the main `ClassLoader` of the application.

A [DynamicClassLoader] can be asked to load these three categories of classes:

1. A class from a `.class` file of the build
2. A class from  a **compile only** dependency `.jar` of the build
3. A class from another dependency `.jar` of the build

Loading of the classes from **compile only** dependencies is delegated to the parent `ClassLoader`.

The classes from `.class` files and ordinary `.jar` files [DynamicClassLoader] loads by itself.

There's also a rule a developer should follow, that only the [API JAR] and its dependencies should be made **compile only**.
So, if everything is correct, then [DynamicClassLoader] will delegate to the main `ClassLoader` only classes from [API JAR] and its dependencies.
