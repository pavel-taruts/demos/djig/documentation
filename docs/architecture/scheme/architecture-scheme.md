# Architecture Diagram

Below is the architecture diagram of an application which uses djig. 

You can have it larger, if you 
<a target="_blank" rel="noopener noreferrer" href="../img/architecture.png">
open it in a separate browser tab</a>.

<a target="_blank" rel="noopener noreferrer" href="../img/architecture.png">
<img src="../img/architecture.png" alt="architecture diagram" />
</a>.

### About the colors and symbols on the diagram

Light-grey 
( <img src="../img/architecture-light.png" width="16px" height="16xp" alt="light-grey square" /> )
indicates well known tools and platforms.
In our case those are the Git repository hosting (like GitLab),
Spring Boot and JRE (ClassLoader in particular),
Git, Gradle and Maven.

Light-blue
( <img src="../img/architecture-light-blue.png" width="16px" height="16xp" alt="light-blue square" /> ) 
is used for user application elements which are not a part of JRE, Spring Boot or djig.

Purple
( <img src="../img/architecture-purple.png" width="16px" height="16xp" alt="purple square" /> )
is for djig runtime components.

Light-purple
( <img src="../img/architecture-light-purple.png" width="16px" height="16xp" alt="light purple square" /> )
is the color of user defined **dynamic components** (their source code, objects, classes and interfaces),
which the user integrates with the main part of the application by using djig.

Also, light-purple
( <img src="../img/architecture-light-purple.png" width="16px" height="16xp" alt="light-purple square" /> )
indicates standard djig objects, 
which are adapters between the dynamic code and the main part of the application.
Those are djig objects directly in touch with the dynamic beans.
Namely, it's 
[CGLIB proxies](../dynamic-proxies.md), 
[DynamicClassLoader](../DynamicClassLoader.md), 
[DynamicApplicationContext](../DynamicApplicationContext.md).

Outlined icons
( <img src="../img/diamonds.png" width="16px" height="16xp" alt="oulined diamonds suite" /> ) 
are interfaces.

Filled icons
( <img src="../img/diamonds-solid.png" width="16px" height="16xp" alt="filled diamonds suite" /> )
are the implementations.
