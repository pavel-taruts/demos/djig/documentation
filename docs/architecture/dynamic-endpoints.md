# Dynamic Endpoints

Dynamic projects can add REST endpoints to the application.

This is called **dynamic endpoints**.

To add a dynamic endpoint a dynamic projects only needs to contain an annotated controller. 

The endpoints of the controller will be added to the application with a path prefix like this:

`/<the value of configuration property djig.controller.dynamic-project.path>/<dynamic project name>/`

For example, let's assume
`djig.controller.dynamic-project.path = "dynamic-projects"`, 
the dynamic project name is `foo`,
and inside the dynamic project there is a controller like this.

```Java
@RestController
@RequestMapping("message")
public class MessageController {
    
    @GetMapping("hello")
    public String hello() {
        return "hello";
    }
}
```

This would add to the application the endpoint `GET /dynamic-projects/foo/message/hello`.
