# API JAR

API JAR is a Maven artifact (a JAR + its dependencies), 
containing interfaces for interaction of a dynamic project and 
the main part of a djig application. 

It's a normal Java or Kotlin project, 
where the JAR is built with Maven or Gradle 
in a usual way.

So, aside from the Git repository of the main part of the application 
and aside from the Git repositories of its dynamic projects, 
there will be one or more Git repositories each with the sources of an API JAR.

Of course there is also an option to have all API JAR sources in the main Git repository.

When an API JAR is modified, 
you have to rebuild and redeploy the main part of the application, 
in order make those changes work.
Although, most probably you won't need to change an API JAR as often as the dynamic project.

The main part of an API JAR is dynamic bean interfaces and the interfaces of normal beans that dynamic beans are allowed to depend on.

An API JAR is both a dependency of the dynamic project and the main part of the application.

If there is an application with several dynamic projects, then it has to depend on several API JARs, 
but a dynamic project can depend on just one API JAR, implementations of the interfaces of which the dynamic project contains. 

An API JAR is a static dependency of the main part of the application and a static dependency of the dynamic project.

Here is the full list of what an API JAR must contain:

- **Interfaces for dynamic beans**. 
  Through those interfaces dynamic beans are injected as Spring dependencies into beans of the main part of the application
  (via [Dynamic proxies](dynamic-proxies.md))
- **Interfaces for beans of the main part of the application, that dynamic beans can use as dependencies**. 
  Those are used to inject beans of the main part of the application into dynamic beans
- **A dependency on org.taruts.djig:djig-dynamic-api**
- If necessary, **dependencies of the main part of the application used by dynamic beans as well** 
  (static dependencies of dynamic beans)

## The org.taruts.djig:djig-dynamic-api artifact

Every API JAR must depend on `org.taruts.djig:djig-dynamic-api`. 
Its contents are the following:

- A dependency on **spring-context**
  (so that a dynamic project could define Spring beans)
- A dependency on **spring-web**
  (so that a dynamic project could define controllers)
- The `org.taruts.djig.dynamicApi.DynamicComponent` interface
  (a marker interface all dynamic bean interfaces must extend)
