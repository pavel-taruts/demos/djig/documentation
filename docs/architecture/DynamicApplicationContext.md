[DynamicClassLoader]: DynamicClassLoader.md
[DynamicApplicationContext]: https://gitlab.com/pavel-taruts/djig/spring-boot-starter/-/blob/master/src/main/java/org/taruts/djig/core/childContext/context/DynamicApplicationContext.java
[API JAR]: api-jar.md

# DynamicApplicationContext

On every change in a dynamic project with the new [DynamicClassLoader] a new [DynamicApplicationContext] is created. 

[DynamicApplicationContext] is the context containing all dynamic beans of the dynamic project,
and it uses [DynamicClassLoader] for class loading.

While loading beans
[DynamicApplicationContext] 
takes into account class level annotations from the `org.springframework.stereotype` package, 
like `@Component`, `@Service`, etc., automatically adding beans into itself.

[DynamicApplicationContext] is a child context of the main context of the application. 
Because of that dynamic beans can be injected with beans from the main part of the application 
if there are corresponding interfaces in the [API JAR] or its dependencies.

Also, a dynamic bean can depend on another dynamic bean, 
if its interface is in the compile classpath of the dynamic project. 
I.e. if the interface is either in the [API JAR], 
in the dependencies of the [API JAR] 
or is defined in the dynamic project itself.
